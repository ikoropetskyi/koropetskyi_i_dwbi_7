﻿--choose database
USE I_K_module_3;
GO

--failed insert into table dbo.Owner
--date of birth is not valid
--check constraint violation
INSERT INTO [dbo].[Owner] (id, passport, first_name, last_name, middle_name, date_of_birth, 
                       city, street_name, street_number, license_number, license_date_from,
					   updated_date)
VALUES ('5522181820','KC670002','Володимир','Салабай','Ярославович','20190620',
        'Шкло','Львівська','56','ФВА1223','20100202',Null);
GO

--failed insert into table dbo.Car
--engine capicity is negative
--check constraint violation
INSERT INTO [dbo].[Car] (id, registration_number, owner_id, mark, body_type, engine_capicity,
                         door_number, cylinder_number, fuel_type, color,
						 registration_date, updated_date)
VALUES ('GTKKSDSJ23N2FІВІВ','АА3423ЛР','3424542333','DEO Lanos','седан',-1400,
        4,12,'бензин','білий','20140609',Null);
GO