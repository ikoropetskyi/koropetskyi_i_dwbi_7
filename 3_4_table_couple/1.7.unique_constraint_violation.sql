﻿--choose database
USE I_K_module_3;
GO

--failed insert into table dbo.Owner
--passport already exists in the table
--unique constraint violation
INSERT INTO [dbo].[Owner] (id, passport, first_name, last_name, middle_name, date_of_birth, 
                       city, street_name, street_number, license_number, license_date_from,
					   updated_date)
VALUES ('4522181820','KC330000','Володимир','Швець','Богданович','19910620',
        'Львів','Городоцька','23','ФВА1223','20100202',Null);
GO

--failed insert into table dbo.Car
--registration number already exists in the table
--unique constraint violation
INSERT INTO [dbo].[Car] (id, registration_number, owner_id, mark, body_type, engine_capicity,
                         door_number, cylinder_number, fuel_type, color,
						 registration_date, updated_date)
VALUES ('LJFKSDSJ23N2FІВІВ','ВС3423ЛР','3424542333','KIA Sportage','хачбек',2500,
         5,12,'дизель','червоний','20120409',Null);
GO