﻿--choose database
USE I_K_module_3;
GO

--drop tables if they already exist
DROP TABLE IF EXISTS HR.employee;
DROP TABLE IF EXISTS HR.employee_audit;
GO

CREATE TABLE HR.employee
  (
  id INT NOT NULL CONSTRAINT PK_employee_id PRIMARY KEY,
  inn char(10) NOT NULL CONSTRAINT AK_employee_inn UNIQUE,
  passport NCHAR(8) NOT NULL,
  first_name NVARCHAR(20) NOT NULL,
  last_name NVARCHAR(30) NOT NULL,
  middle_name NVARCHAR(25) NOT NULL,
  date_of_birth DATE NOT NULL,
  date_of_hire DATE NOT NULL,
  date_of_fire DATE NULL,
  department NVARCHAR(20) NOT NULL DEFAULT('HR'),
  position NVARCHAR(20) NULL,
  salary numeric(10,2) NOT NULL CONSTRAINT CK_minimal_salary CHECK(salary>=3723.00)
  );
GO

CREATE TABLE HR.employee_audit
  (
  id INT NOT NULL,
  inn char(10) NOT NULL,
  passport NCHAR(8) NOT NULL,
  first_name NVARCHAR(20) NOT NULL,
  last_name NVARCHAR(30) NOT NULL,
  middle_name NVARCHAR(25) NOT NULL,
  date_of_birth DATE NOT NULL,
  date_of_hire DATE NOT NULL,
  date_of_fire DATE NULL,
  department NVARCHAR(20) NOT NULL,
  position NVARCHAR(20) NULL,
  salary numeric(10,2) NOT NULL,
  operation_type CHAR(10) NOT NULL,
  operation_date DATETIME NOT NULL
  );
GO