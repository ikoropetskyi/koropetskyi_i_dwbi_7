﻿--choose database
USE I_K_module_3;
GO

--failed insert into table dbo.Owner
--id already exists in the table
--primary key violation
INSERT INTO [dbo].[Owner] (id, passport, first_name, last_name, middle_name, date_of_birth, 
                       city, street_name, street_number, license_number, license_date_from,
					   updated_date)
VALUES ('3424542333','ТИ600608','Ігор','Коропецький','Петрович','19910831',
        'Шкло','Шевченка','12','ДЛШ9812','20120602',Null);
GO

--failed insert into table dbo.Car
--id already exists in the table
--primary key violation
INSERT INTO [dbo].[Car] (id, registration_number, owner_id, mark, body_type, engine_capicity,
                         door_number, cylinder_number, fuel_type, color,
						 registration_date, updated_date)
VALUES ('EBFKSDSJ23N2FІВІВ','ВС6995ЛМ','3424542333','KIA Sportage','хачбек',2500,
         5,12,'дизель','червоний','20120409',Null);
GO