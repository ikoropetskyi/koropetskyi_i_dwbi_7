﻿--choose database
USE I_K_module_3;
GO

--select data before update
SELECT * FROM HR.employee;

--updating
UPDATE HR.employee
SET last_name='ОНОВЛЕННЯ'
WHERE id=1;

--select data after update
SELECT * FROM HR.employee;
GO

--select data before delete
SELECT * FROM HR.employee;

--deleting
DELETE FROM HR.employee
WHERE id=2;

--select data after deleting
SELECT * FROM HR.employee;
GO

--select data from audit table
SELECT * FROM HR.employee_audit;
GO