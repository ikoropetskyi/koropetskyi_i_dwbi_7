﻿--choose database
USE I_K_module_3;
GO

--successful insert into table dbo.Car
--door_number is omitted, but default value is inserted
--default value checking
INSERT INTO [dbo].[Car] (id, registration_number, owner_id, mark, body_type, engine_capicity,
                         cylinder_number, fuel_type, color,
						 registration_date, updated_date)
VALUES ('LNYKSDSJ23N2FІВІВ','АІ5623ЛР','3322181820','Honda CRV','хачбек',2000,
        12,'бензин','білий','20140609',Null);
GO

--select data
SELECT * FROM [dbo].[Car];
GO