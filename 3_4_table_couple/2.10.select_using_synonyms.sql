﻿USE education;

SELECT * FROM I_Koropetskyi.emp;
SELECT * FROM I_Koropetskyi.emp_audit;


--first query
SELECT SUM(salary) AS total_salary
FROM I_Koropetskyi.emp;

--second query
SELECT id, COUNT(*) AS [operation_number] 
FROM I_Koropetskyi.emp_audit
GROUP BY id;

--third query
SELECT id, last_name, first_name
FROM I_Koropetskyi.emp_audit

EXCEPT

SELECT id, last_name, first_name
FROM I_Koropetskyi.emp;

--forth query
SELECT *
FROM I_Koropetskyi.emp
WHERE salary>10000;