﻿--choose database
USE I_K_module_3;
GO

--DEFAULT CHECKING
--department is omitted, but default value is inserted

INSERT INTO HR.employee(id,inn,passport,first_name,last_name,middle_name,date_of_birth,
			  date_of_hire,date_of_fire,position,salary)
VALUES (3,'3333333333','КС333333','Артем','Мірзоян','Іванович','19920106','20110626',NULL,'обліковець',11000.00);

--select from main table
SELECT * FROM HR.employee;

--SELECT FROM audit table
SELECT * FROM HR.employee_audit;

GO