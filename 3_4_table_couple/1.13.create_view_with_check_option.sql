﻿--choose DB
USE I_K_module_3;
GO

DROP VIEW IF EXISTS [dbo].[car_extended_info];
GO

--view for dbo.car table with check option
CREATE VIEW [dbo].[car_extended_info](registration_number,mark,color) 
AS
SELECT registration_number,mark,color
FROM [dbo].[car]
WHERE color='чорний'
WITH CHECK OPTION;
GO

--select data from view
SELECT * FROM [dbo].[car_extended_info];
GO

--try to update car color
UPDATE [dbo].[car_extended_info]
  SET color='червоний'
WHERE registration_number='ВС3423ЛР';
GO