﻿--choose database
USE I_K_module_3;
GO

--successful insert into tables
--insert into dbo.Owner
INSERT INTO [dbo].[Owner] (id, passport, first_name, last_name, middle_name, date_of_birth, 
                       city, street_name, street_number, license_number, license_date_from,
					   updated_date)
VALUES ('3322181820','KC330000','Володимир','Швець','Богданович','19910620',
        'Львів','Городоцька','23','ФВА1223','20100202',Null),
		('3424542333','КД600608','Ігор','Коропецький','Петрович','19910831',
		 'Шкло','Шевченка','12',Null,Null,Null);

--selct data from dbo.Owner table
SELECT * FROM [dbo].[Owner];

--insert into dbo.Car
INSERT INTO [dbo].[Car] (id, registration_number, owner_id, mark, body_type, engine_capicity,
                         door_number, cylinder_number, fuel_type, color,
						 registration_date, updated_date)
VALUES ('WBFKSD4J23N23ІВІВ','ВС3423ЛР','3322181820','OPEL Vectra','седан',1800,
        4,12,'бензин','чорний','20100112',Null),
		('EBFKSDSJ23N2FІВІВ','ВС4524ЛР','3424542333','KIA Sportage','хачбек',2500,
         5,12,'дизель','червоний','20120409',Null);

--selct data from dbo.Car table
SELECT * FROM [dbo].[Car];
GO
