﻿--choose DB
USE I_K_module_3;
GO

DROP TRIGGER IF EXISTS set_owner_update_column_value;
DROP TRIGGER IF EXISTS set_car_update_column_value;
GO

--create triger for table dbo.owner
CREATE TRIGGER set_owner_update_column_value ON [dbo].[owner]
AFTER UPDATE
AS
IF @@ROWCOUNT=0 RETURN;
UPDATE [dbo].[owner]
  SET updated_date=CURRENT_TIMESTAMP
  FROM [dbo].[owner] AS O 
    INNER JOIN inserted
	  ON O.id=inserted.id;
GO

--create trigger for table dbo.car
CREATE TRIGGER set_car_update_column_value ON [dbo].[car]
AFTER UPDATE
AS
IF @@ROWCOUNT=0 RETURN;
UPDATE [dbo].[car]
  SET updated_date=CURRENT_TIMESTAMP
  FROM [dbo].[car] AS C 
    INNER JOIN inserted
	  ON C.id=inserted.id;
GO