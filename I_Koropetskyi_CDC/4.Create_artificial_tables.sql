--CREATE ARTIFICIAL TABLES--
USE I_Koropetskyi_CDC;
GO

IF OBJECT_ID('dbo.Koropetskyi_DimCurrency_CDC_updates','U') IS NOT NULL
  DROP TABLE dbo.Koropetskyi_DimCurrency_CDC_updates;
GO

CREATE TABLE dbo.Koropetskyi_DimCurrency_CDC_updates
    ([CurrencyKey] [int] NOT NULL,
	[CurrencyAlternateKey] [nchar](3) NOT NULL,
	[CurrencyName] [nvarchar](50) NOT NULL)
GO


IF OBJECT_ID('dbo.Koropetskyi_DimCurrency_CDC_deletes','U') IS NOT NULL
  DROP TABLE dbo.Koropetskyi_DimCurrency_CDC_deletes;
GO

CREATE TABLE dbo.Koropetskyi_DimCurrency_CDC_deletes
    ([CurrencyKey] [int] NOT NULL,
	[CurrencyAlternateKey] [nchar](3) NOT NULL,
	[CurrencyName] [nvarchar](50) NOT NULL)
GO


IF OBJECT_ID('dbo.Koropetskyi_DimCurrency_CDC_inserts','U') IS NOT NULL
  DROP TABLE dbo.Koropetskyi_DimCurrency_CDC_inserts;
GO

CREATE TABLE dbo.Koropetskyi_DimCurrency_CDC_inserts
    ([CurrencyKey] [int] NOT NULL,
	[CurrencyAlternateKey] [nchar](3) NOT NULL,
	[CurrencyName] [nvarchar](50) NOT NULL)
GO