--drop test database if exists
IF db_id('I_Koropetskyi_CDC') IS NOT NULL
DROP DATABASE [I_Koropetskyi_CDC];
GO

--create test database--
CREATE DATABASE [I_Koropetskyi_CDC];
GO

--CREATE SOURCE TABLE 
USE [I_Koropetskyi_CDC];
GO
IF OBJECT_ID('dbo.Koropetskyi_DimCurrency_CDC','U') IS NOT NULL
  DROP TABLE [dbo].[Koropetskyi_DimCurrency_CDC];
GO

SELECT *
INTO [dbo].[Koropetskyi_DimCurrency_CDC]
FROM [AdventureWorksDW2014].[dbo].[DimCurrency]
WHERE CurrencyKey>=10 and CurrencyKey<=40;
GO

--ENABLE CDC ON DATABASE--
USE [I_Koropetskyi_CDC];
GO

EXEC sys.sp_cdc_enable_db;
GO

--CREATE PRIMARY KEY ON SOURCE TABLE
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID('[dbo].[Koropetskyi_DimCurrency_CDC]') AND [name] = 'PK_Koropetskyi_DimCurrency_CDC')
  ALTER TABLE [dbo].[Koropetskyi_DimCurrency_CDC] ADD CONSTRAINT [PK_Koropetskyi_DimCurrency_CDC] PRIMARY KEY CLUSTERED
(
    [CurrencyKey]
);
GO

--ENABLE CDC ON SOURCE TABLE--
EXEC sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'Koropetskyi_DimCurrency_CDC',
@role_name = N'cdc_Admin',
@supports_net_changes = 1;

GO