﻿use triger_fk_cursor;
go

--post employee fk, cascade
drop trigger if exists fk_post_employee_father_delete;
go

drop trigger if exists fk_post_employee_father_update;
go

drop trigger if exists fk_post_employee_doughter;
go

create trigger fk_post_employee_father_delete on post
after delete
as
	begin
		delete from employee
		from deleted 
		  inner join employee on deleted.post=employee.post;
	end;
go

-- ми не можемо встановити точну відповідність між
-- рядками таблиць inserted і deleted, оскільки немає іншого стовпця унікальних значень,
-- який би міг однозначно ідентифікувати(співставляти) змінені рядки
-- використання курсора також неможливе, оскільки згідно
-- теорії множин порядок рядків у двох таблицях може бути довільним
-- оскільки апдейтиться значення первинного ключа (в принципі це вказує на проблеми проектування)
-- то приймаємо принцип: якщо змінюється одне значення, то відбуваються каскадні зміни в таблиці-дочці
-- якщо змінюється 2 і більше значень - відкатуємо назад ТЗ  
create trigger fk_post_employee_father_update on post
after update
as
	begin
	    if @@ROWCOUNT>1
		  begin
		    print 'you can not change more then 1 value at once';
			rollback tran;
		  end;
		update employee
		  set employee.post=(select top 1 post from inserted)
		  where employee.post=(select top 1 post from deleted);
	end;
go



create trigger fk_post_employee_doughter on employee
after insert, update
as
	begin
		if exists ((select post from inserted) except (select post from post))
			begin
				print 'fk violation on employee post'
				rollback tran;
			end;
	end;
go


----------

--street pharmacy fk, set null
drop trigger if exists fk_street_pharmacy_father;
go

drop trigger if exists fk_street_pharmacy_doughter;
go

create trigger fk_street_pharmacy_father on street
after delete,update
as
	begin
		update pharmacy
		set pharmacy.street=null
		from deleted 
		  inner join pharmacy on deleted.street=pharmacy.street
	end;
go

create trigger fk_street_pharmacy_doughter on pharmacy
after insert, update
as
	begin
		if exists ((select street from inserted where street is not null) except (select street from street))
			begin
				print 'fk violation on street pharmacy'
				rollback tran;
			end;
	end;
go

--pharmacy employee fk, set null
drop trigger if exists fk_pharmacy_employee_father;
go


drop trigger if exists fk_pharmacy_employee_doughter;
go

create trigger fk_pharmacy_employee_father on pharmacy
after delete
as
	begin
		update employee
		set employee.pharmacy_id=null
		from deleted 
		  inner join employee on deleted.id=employee.pharmacy_id
	end;
go

--немає потреби робити тригер на апдейт, оскільки стовпець
--на який йде посилання є сурогатним, до того ж - це первинний ключ,
--також це є identity


create trigger fk_pharmacy_employee_doughter on employee
after insert, update
as
	begin
		if exists ((select pharmacy_id from inserted where pharmacy_id is not null) except (select id from pharmacy))
			begin
				print 'fk violation on pharmacy employee'
				rollback tran;
			end;
	end;
go

--zone medicine_zone fk, cascade
drop trigger if exists fk_zone_medicine_zone_father;
go

drop trigger if exists fk_zone_medicine_zone_doughter;
go

create trigger fk_zone_medicine_zone_father on [zone]
after delete
as
	begin
		delete from medicine_zone
		from deleted 
		  inner join medicine_zone on deleted.id=medicine_zone.zone_id
	end;
go

--немає потреби робити тригер на апдейт, оскільки стовпець
--на який йде посилання є сурогатним, до того ж - це первинний ключ,
--також це є identity

create trigger fk_zone_medicine_zone_doughter on medicine_zone
after insert, update
as
	begin
		if exists ((select zone_id from inserted) except (select id from [zone]))
			begin
				print 'fk violation on zone medicine zone'
				rollback tran;
			end;
	end;
go

--medicine medicine_zone fk, restrict
drop trigger if exists fk_medicine_medicine_zone_father;
go

drop trigger if exists fk_medicine_medicine_zone_doughter;
go

create trigger fk_medicine_medicine_zone_father on medicine
after delete
as
	begin
		if exists ((select id from deleted) intersect (select medicine_id from medicine_zone))
			begin
				print 'fk violation on medicine medicine_zone'
				rollback tran;
			end;
	end;
go

create trigger fk_medicine_medicine_zone_doughter on medicine_zone
after insert, update
as
	begin
		if exists ((select medicine_id from inserted) except (select id from medicine))
			begin
				print 'fk violation on medicine medicine_zone'
				rollback tran;
			end;
	end;
go

--medicine pharmacy_medicine fk, restrict
drop trigger if exists fk_medicine_pharmacy_medicine_father;
go


drop trigger if exists fk_medicine_pharmacy_medicine_doughter;
go

create trigger fk_medicine_pharmacy_medicine_father on medicine
after delete
as
	begin
		if exists ((select id from deleted) intersect (select medicine_id from pharmacy_medicine))
			begin
				print 'fk violation on medicine_pharmacy_medicine'
				rollback tran;
			end;
	end;
go

create trigger fk_medicine_pharmacy_medicine_doughter on pharmacy_medicine
after insert, update
as
	begin
		if exists ((select medicine_id from inserted) except (select id from medicine))
			begin
				print 'fk violation on medicine_pharmacy_medicine'
				rollback tran;
			end;
	end;
go

--pharmacy pharmacy_medicine fk, restrict
drop trigger if exists fk_pharmacy_pharmacy_medicine_father;
go


drop trigger if exists fk_pharmacy_pharmacy_medicine_doughter;
go

create trigger fk_pharmacy_pharmacy_medicine_father on pharmacy
after delete
as
	begin
		if exists ((select id from deleted) intersect (select pharmacy_id from pharmacy_medicine))
			begin
				print 'fk violation on pharmacy_pharmacy_medicine'
				rollback tran;
			end;
	end;
go

create trigger fk_pharmacy_pharmacy_medicine_doughter on pharmacy_medicine
after insert, update
as
	begin
		if exists ((select pharmacy_id from inserted) except (select id from pharmacy))
			begin
				print 'fk violation on pharmacy_pharmacy_medicine'
				rollback tran;
			end;
	end;
go
---------------
--trigger identity_number last two symbols not 00

drop trigger if exists not_00_identity_number;
go

create trigger not_00_identity_number on employee
after insert, update
as
	begin
		if exists(select 1 from inserted where identity_number like '%00')
			begin
				print 'not correct identity number'
				rollback tran
			end;

	end;
go

--trigger for ministry_code pattern

drop trigger if exists ministry_code_pattern;
go

create trigger ministry_code_pattern on medicine
after insert, update
as
	begin
		if exists(select 1 from inserted where ministry_code not like '[a-lnoq-z][a-lnoq-z]-[0-9][0-9][0-9]-[0-9][0-9]')
			begin
				print 'not correct ministry_code'
				rollback tran
			end;

	end;
go
--trigger for post delete restriction

drop trigger if exists post_not_delete;
go

create trigger post_not_delete on post
after delete
as
	begin
		print 'delete operation is forbidden';
		rollback tran;
	end;


