﻿--1--
USE labor_sql;
SELECT pr.maker, pr.[type], pc.speed, pc.hd
FROM pc
  INNER JOIN product AS pr
    ON pc.model=pr.model
WHERE pc.hd<=8;

--2--
USE labor_sql;
SELECT DISTINCT pr.maker
FROM pc
  INNER JOIN product AS pr
    ON pc.model=pr.model
WHERE pc.speed>=600;

--3--
USE labor_sql;
SELECT DISTINCT pr.maker
FROM laptop as l
  INNER JOIN product AS pr
    ON l.model=pr.model
WHERE l.speed<=500;

--4--
USE labor_sql;
SELECT DISTINCT l1.model AS model_1, l2.model AS model_2, l1.hd, l1.ram
FROM laptop AS l1
  INNER JOIN laptop AS l2
    ON l1.hd=l2.hd AND l1.ram=l2.ram AND l1.model>l2.model;

--5--
USE labor_sql;
SELECT c1.country, c1.class AS bb_class, c2.class as bc_class
from classes AS c1
  INNER JOIN classes AS c2
    ON c1.country=c2.country
WHERE c1.[type]='bb' AND c2.[type]='bc';

--6--
USE labor_sql;
SELECT DISTINCT pc.model, pr.maker
FROM pc
  INNER JOIN product AS pr
    ON pc.model=pr.model
WHERE pc.price<600;

--7--
USE labor_sql;
SELECT DISTINCT p.model, pr.maker
FROM printer AS p
  INNER JOIN product AS pr
    ON p.model=pr.model
WHERE p.price>300;

--8--
USE labor_sql;
SELECT pr.maker, pc.model, pc.price
FROM pc
  INNER JOIN product AS pr
    ON pc.model=pr.model;

--9--
USE labor_sql;
SELECT pr.maker, pr.model, pc.price
FROM product AS pr
  LEFT JOIN pc
    ON pc.model=pr.model
WHERE pr.[type]='pc';

--10--
USE labor_sql;
SELECT pr.maker, pr.[type], l.model, l.speed
FROM laptop AS l
  INNER JOIN product AS pr
    ON l.model=pr.model
WHERE l.speed>600;

--11--
USE labor_sql;
SELECT s.[name], c.displacement
FROM ships AS s
  INNER JOIN classes AS c
    ON s.class=c.class;

--12--
--варіант 1 - вивести деталі битв, якщо корабель вцілів у цій битві
USE labor_sql;
SELECT o.ship, b.[name], b.[date]
FROM outcomes AS o
  INNER JOIN battles AS b
    ON o.battle=b.[name]
WHERE o.result='OK';

--варіант 2 - вивести деталі битв для кораблів, які  вціліли у всіх битвах, де брали участь
USE labor_sql;
SELECT o.ship, b.[name], b.[date]
FROM outcomes AS o
  INNER JOIN battles AS b
    ON o.battle=b.[name]
WHERE NOT EXISTS(SELECT * FROM outcomes AS o2 WHERE (o2.result='sunk' OR o2.result='damaged') AND o2.ship=o.ship);

--13--
USE labor_sql;
SELECT s.[name], c.country
FROM ships AS s
  INNER JOIN classes AS c
    ON s.class=c.class;

--14--
USE labor_sql;
SELECT DISTINCT t.plane, c.[name] AS company_name
FROM trip AS t
  INNER JOIN company AS c
    ON t.id_comp=c.id_comp
WHERE t.plane='Boeing';

--15--
USE labor_sql;
SELECT DISTINCT p.id_psg, p.[name], pit.[date]
FROM passenger AS p
  INNER JOIN pass_in_trip AS pit
    ON p.id_psg=pit.id_psg;

--16--
USE	labor_sql;
SELECT pc.model, pc.speed, pc.hd
FROM pc
  INNER JOIN product AS pr
    ON pc.model=pr.model
WHERE (pc.hd=10 OR pc.hd=20) AND pr.maker='A'
ORDER BY pc.speed;

--17--
USE labor_sql;
SELECT *
FROM product
 PIVOT (COUNT(model) 
        FOR [type] in (pc, laptop, printer)) AS p;

--18--
USE labor_sql;
--v1
SELECT *
FROM (SELECT screen, price, 'avarage price' AS _avg
      FROM laptop) AS l
 PIVOT (AVG(price)
        FOR screen IN ([11],[12],[14],[15])) AS P;

--v2
DECLARE @sql AS NVARCHAR(MAX);
DECLARE @columns AS NVARCHAR(MAX);

SELECT @columns = ISNULL(@columns + ',','') + QUOTENAME(screen)
FROM (SELECT DISTINCT screen FROM laptop) AS l;
 
SET @sql= 
  N'SELECT *
    FROM (SELECT screen, price, ''average price'' AS _avg 
	FROM laptop) AS l
    PIVOT(AVG(price) 
          FOR screen IN (' + @columns + ')) AS P';

EXECUTE (@sql);
go

--19--
USE labor_sql;
SELECT * 
FROM laptop AS l
  CROSS APPLY (SELECT maker
               FROM product AS pr
			   WHERE pr.model=l.model) AS m;

--20--
USE labor_sql;
SELECT * 
FROM laptop AS l1
  CROSS APPLY (SELECT MAX(l2.price) AS max_price
               FROM laptop AS l2
				 INNER JOIN product AS pr1
				   ON l2.model=pr1.model
			   WHERE pr1.maker=(SELECT pr2.maker 
								FROM product AS pr2	
								WHERE pr2.model=l1.model)) AS m;

--21--
USE labor_sql;
WITH c
AS
(SELECT code, model, speed, ram, hd, price, screen, 
        ROW_NUMBER() OVER(ORDER BY model, code) AS row_num
FROM laptop)

SELECT c1.code, c1.model, c1.speed, c1.ram, c1.hd, c1.price, c1.screen,
       l.code, l.model, l.speed, l.ram, l.hd, l.price, l.screen
FROM c AS c1
  CROSS APPLY (SELECT TOP 1 *
               FROM c AS c2
			   WHERE c2.row_num>c1.row_num
			   ORDER BY c2.row_num) AS l 
ORDER BY c1.model,c1.code;

--22--
USE labor_sql;
WITH c
AS
(SELECT code, model, speed, ram, hd, price, screen, 
        ROW_NUMBER() OVER(ORDER BY model, code) AS row_num
FROM laptop)

SELECT c1.code, c1.model, c1.speed, c1.ram, c1.hd, c1.price, c1.screen,
       l.code, l.model, l.speed, l.ram, l.hd, l.price, l.screen
FROM c AS c1
  OUTER APPLY (SELECT TOP 1 *
               FROM c AS c2
			   WHERE c2.row_num>c1.row_num
			   ORDER BY c2.row_num) AS l 
ORDER BY c1.model,c1.code;

--23--
USE labor_sql;
SELECT t2.maker, t2.model, t1.[type]
FROM (SELECT DISTINCT p1.[type]
      FROM product as p1) AS t1
  CROSS APPLY (SELECT TOP 3 p2.maker, p2.model
               FROM product as p2
			   WHERE p2.[type]=t1.[type]
			   ORDER BY p2.model) AS t2;

--24--
USE labor_sql;
SELECT code, [name], [value]
FROM (SELECT code, speed, ram, CAST(hd AS smallint) AS hd, CAST(screen AS smallint) as screen
      FROM laptop) AS l
  UNPIVOT ([value] FOR [name] IN (speed, ram, hd, screen)) AS u;
