﻿USE I_Koropetskyi_Library;
GO
----------------------
ALTER TABLE Authors
DROP COLUMN IF EXISTS birthday;

ALTER TABLE Authors
DROP COLUMN IF EXISTS book_amount;

ALTER TABLE Authors
DROP COLUMN IF EXISTS issue_amount;

ALTER TABLE Authors
DROP COLUMN IF EXISTS total_edition;


ALTER TABLE Authors
ADD birthday DATE NULL,
    book_amount SMALLINT NOT NULL DEFAULT(0) CHECK(book_amount>=0),
	issue_amount SMALLINT NOT NULL DEFAULT(0) CHECK(issue_amount>=0),
	total_edition INT NOT NULL DEFAULT(0) CHECK(total_edition>=0);

-------------------------------

ALTER TABLE Books
DROP COLUMN IF EXISTS title;

ALTER TABLE Books
DROP COLUMN IF EXISTS edition;

ALTER TABLE Books
DROP COLUMN IF EXISTS published;

ALTER TABLE Books
DROP COLUMN IF EXISTS issue;


ALTER TABLE Books
ADD title NVARCHAR(50) NOT NULL DEFAULT('Title'),
edition INT NOT NULL DEFAULT(1) CHECK(edition>=1),
published DATE NULL,
issue TINYINT NOT NULL DEFAULT(1);

----------------

ALTER TABLE Publishers
DROP COLUMN IF EXISTS created;

ALTER TABLE Publishers
DROP COLUMN IF EXISTS country;

ALTER TABLE Publishers
DROP COLUMN IF EXISTS City;

ALTER TABLE Publishers
DROP COLUMN IF EXISTS book_amount;

ALTER TABLE Publishers
DROP COLUMN IF EXISTS issue_amount;

ALTER TABLE Publishers
DROP COLUMN IF EXISTS total_edition;



ALTER TABLE Publishers
ADD created DATE DEFAULT('19000101') NOT NULL,
country NVARCHAR(20) NOT NULL DEFAULT('USA'),
City NVARCHAR(20) NOT NULL DEFAULT('NY'),
book_amount INT NOT NULL DEFAULT(0) CHECK(book_amount>=0),
issue_amount INT NOT NULL DEFAULT(0) CHECK(issue_amount>=0),
total_edition INT NOT NULL DEFAULT(0) CHECK(total_edition>=0);

-------------------------------

ALTER TABLE Authors_log
DROP COLUMN IF EXISTS book_amount_old;

ALTER TABLE Authors_log
DROP COLUMN IF EXISTS issue_amount_old;

ALTER TABLE Authors_log
DROP COLUMN IF EXISTS total_edition_old;

ALTER TABLE Authors_log
DROP COLUMN IF EXISTS book_amount_new;

ALTER TABLE Authors_log
DROP COLUMN IF EXISTS issue_amount_new;

ALTER TABLE Authors_log
DROP COLUMN IF EXISTS total_edition_new;


ALTER TABLE Authors_log
ADD book_amount_old SMALLINT NULL,
    issue_amount_old SMALLINT NULL,
	total_edition_old INT NULL,
	book_amount_new SMALLINT NULL,
	issue_amount_new SMALLINT NULL,
	total_edition_new INT NULL;


DECLARE @pk_name AS VARCHAR(100);

SET @pk_name=(SELECT name FROM sys.key_constraints  
WHERE type = 'PK' AND OBJECT_NAME(parent_object_id) = N'Books');

EXEC sp_rename @pk_name,'PK_Books';

ALTER TABLE BooksAuthors
DROP CONSTRAINT FK_Books_Authors_ISBN;

ALTER TABLE Books
DROP CONSTRAINT PK_Books;

ALTER TABLE Books
ADD CONSTRAINT PK_Books_New
PRIMARY KEY (ISBN,issue);

/*ALTER TABLE Books
ADD CONSTRAINT UK_ISBN
UNIQUE(ISBN);

ALTER TABLE BooksAuthors
 ADD CONSTRAINT FK_Books_Authors_ISBN_New
 FOREIGN KEY(ISBN) REFERENCES Books(ISBN)
 ON UPDATE CASCADE
 ON DELETE NO ACTION;*/

GO


ALTER trigger trg_populate_authors_logs ON Authors
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end

---- insert
if @operation = 'I'
begin
	insert into Authors_log
				(Author_Id_new,Name_new,URL_new,operation_type,
				book_amount_new,issue_amount_new,total_edition_new)
	Select 
		inserted.Author_Id,
		inserted.Name,
		inserted.URL,
		@operation,
		inserted.book_amount,
		inserted.issue_amount,
		inserted.total_edition

	FROM Authors 
	inner join inserted on Authors.Author_Id = inserted.Author_Id
end

---- delete
if @operation = 'D'
begin
	insert into Authors_log
				(Author_Id_old,Name_old,URL_old,operation_type,
				book_amount_old,issue_amount_old,total_edition_old)
	Select 
		deleted.Author_Id,
		deleted.Name,
		deleted.URL,
		@operation,
		deleted.book_amount,
		deleted.issue_amount,
		deleted.total_edition
	FROM deleted
end

--- update
if 'www.name.com'=ALL(SELECT [URL] FROM deleted) RETURN
if @operation = 'U'
	begin
		update Authors 
		set updated = CURRENT_TIMESTAMP, 
			updated_by = SYSTEM_USER
		from Authors 
			inner join inserted on Authors.Author_Id = inserted.Author_Id;

	insert into Authors_log
				(Author_Id_new,Name_new,URL_new,Author_Id_old,Name_old,URL_old,operation_type,
				book_amount_new,issue_amount_new,total_edition_new,
				book_amount_old,issue_amount_old,total_edition_old)
	Select 
		inserted.Author_Id,
		inserted.Name,
		inserted.URL,
		deleted.Author_Id,
		deleted.Name,
		deleted.URL,
		@operation,
		inserted.book_amount,
		inserted.issue_amount,
		inserted.total_edition,
		deleted.book_amount,
		deleted.issue_amount,
		deleted.total_edition
	FROM inserted INNER JOIN deleted ON inserted.Author_Id = deleted.Author_Id
		
	end
END;
GO

EXEC sp_configure 'show advanced options', 1;
GO
RECONFIGURE ;
GO
EXEC sp_configure 'nested triggers', 1 ;
GO
RECONFIGURE;
GO





