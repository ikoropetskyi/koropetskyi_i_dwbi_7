﻿USE I_Koropetskyi_Library;
GO

DROP TRIGGER IF EXISTS book_amount_definition_for_authors_bookauthors;
GO

DROP TRIGGER IF EXISTS book_amount_definition_for_authors_books_insert_delete;
GO

DROP TRIGGER IF EXISTS book_amount_definition_for_authors_books_update;
GO

CREATE trigger book_amount_definition_for_authors_bookauthors ON BooksAuthors
AFTER delete, insert, update
AS
BEGIN
	UPDATE Authors
	  SET Authors.book_amount=COALESCE((SELECT COUNT(DISTINCT(b.ISBN)) FROM Books as b
	                                  inner join BooksAuthors as ba
									    on b.ISBN=ba.ISBN and ba.Author_id=Authors.Author_id),0),

	      Authors.issue_amount=Authors.issue_amount+COALESCE((SELECT COUNT(*)
															  FROM inserted AS i
																INNER JOIN Books AS b
																	ON i.ISBN=b.ISBN AND i.Author_Id=Authors.Author_id),0)
											       -COALESCE((SELECT COUNT(*)
															  FROM deleted AS d
																INNER JOIN Books AS b
																	ON d.ISBN=b.ISBN AND d.Author_Id=Authors.Author_id),0),

          Authors.total_edition=Authors.total_edition+COALESCE((SELECT SUM(B.edition)
															  FROM inserted AS i
																INNER JOIN Books AS b
																	ON i.ISBN=b.ISBN AND i.Author_Id=Authors.Author_id),0)
											       -COALESCE((SELECT SUM(B.edition)
															  FROM deleted AS d
																INNER JOIN Books AS b
																	ON d.ISBN=b.ISBN AND d.Author_Id=Authors.Author_id),0)

	WHERE Authors.Author_id IN (select Author_id from inserted
								
								UNION
								
								select Author_id from deleted);

end;
go
---------------------------------------------------------------------------------
CREATE trigger book_amount_definition_for_authors_books_insert_delete ON Books
AFTER INSERT, DELETE
AS
BEGIN

	UPDATE Authors
	SET Authors.book_amount=COALESCE((SELECT COUNT(DISTINCT b.ISBN) 
									  FROM Books AS b
	                                    INNER JOIN BooksAuthors AS bo
										  ON b.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0),

        Authors.issue_amount=Authors.issue_amount+COALESCE((SELECT COUNT(*) 
															FROM inserted as i
															  INNER JOIN BooksAuthors AS bo
															    ON i.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0)
												 -COALESCE((SELECT COUNT(*) 
															FROM deleted as d
															  INNER JOIN BooksAuthors AS bo
															    ON d.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0),

		                                                                   
        Authors.total_edition=Authors.total_edition+COALESCE((SELECT SUM(i.edition) 
															FROM inserted as i
															  INNER JOIN BooksAuthors AS bo
															    ON i.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0)
												 -COALESCE((SELECT SUM(d.edition) 
															FROM deleted as d
															  INNER JOIN BooksAuthors AS bo
															    ON d.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0)

	WHERE Authors.Author_id IN (select bo.Author_id 
	                            from inserted AS i 
								  INNER JOIN BooksAuthors AS bo
								    ON i.ISBN=bo.ISBN
								
								UNION
								
								select bo.Author_id 
	                            from deleted AS d 
								  INNER JOIN BooksAuthors AS bo
								    ON d.ISBN=bo.ISBN);
END;
GO

----------------------------------------------------------------------------
CREATE trigger book_amount_definition_for_authors_books_update ON Books
AFTER update
AS
BEGIN
	if update(edition) AND NOT update(ISBN)
	begin
	  UPDATE Authors
	  SET Authors.total_edition=Authors.total_edition+COALESCE((SELECT SUM(i.edition) 
															    FROM inserted as i
															      INNER JOIN BooksAuthors AS bo
															        ON i.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0)
												     -COALESCE((SELECT SUM(d.edition) 
															    FROM deleted as d
															      INNER JOIN BooksAuthors AS bo
															        ON d.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0)
		WHERE Authors.Author_id IN (select bo.Author_id 
	                            from inserted AS i 
								  INNER JOIN BooksAuthors AS bo
								    ON i.ISBN=bo.ISBN
								
								UNION
								
								select bo.Author_id 
	                            from deleted AS d 
								  INNER JOIN BooksAuthors AS bo
								    ON d.ISBN=bo.ISBN);
	end;

	if update(ISBN)
	begin
      UPDATE Authors
	  SET Authors.book_amount=COALESCE((SELECT COUNT(DISTINCT b.ISBN) 
									  FROM Books AS b
	                                    INNER JOIN BooksAuthors AS bo
										  ON b.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0),

          Authors.issue_amount=Authors.issue_amount+COALESCE((SELECT COUNT(*) 
															  FROM inserted as i
															    INNER JOIN BooksAuthors AS bo
															      ON i.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0)
												   -COALESCE((SELECT COUNT(*) 
															  FROM deleted as d
															    INNER JOIN BooksAuthors AS bo
															      ON d.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0),

		                                                                   
          Authors.total_edition=Authors.total_edition+COALESCE((SELECT SUM(i.edition) 
															    FROM inserted as i
															      INNER JOIN BooksAuthors AS bo
															        ON i.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0)
												     -COALESCE((SELECT SUM(d.edition) 
															    FROM deleted as d
															      INNER JOIN BooksAuthors AS bo
															        ON d.ISBN=bo.ISBN AND bo.Author_id=Authors.Author_id),0)

	  WHERE Authors.Author_id IN (select bo.Author_id 
	                              from inserted AS i 
								    INNER JOIN BooksAuthors AS bo
								      ON i.ISBN=bo.ISBN
								
								UNION
								
								select bo.Author_id 
	                            from deleted AS d 
								  INNER JOIN BooksAuthors AS bo
								    ON d.ISBN=bo.ISBN);

	END;
END;
GO