﻿USE I_Koropetskyi_Library;
GO

DROP TRIGGER IF EXISTS book_amount_definition_insert_delete;
GO

DROP TRIGGER IF EXISTS book_amount_definition_update;
GO



CREATE trigger book_amount_definition_insert_delete ON Books
AFTER INSERT, DELETE
AS
BEGIN

	UPDATE Publishers
	SET Publishers.book_amount=COALESCE((SELECT COUNT(DISTINCT Books.ISBN) FROM Books
	                            WHERE Books.Publisher_Id=Publishers.Publisher_Id),0),

        Publishers.issue_amount=Publishers.issue_amount+COALESCE((SELECT COUNT(*) FROM inserted 
	                            WHERE inserted.Publisher_Id=Publishers.Publisher_Id),0)-
	                            COALESCE((SELECT COUNT(*) FROM deleted WHERE deleted.Publisher_Id=Publishers.Publisher_Id),0),

        Publishers.total_edition=(Publishers.total_edition+COALESCE((SELECT SUM(inserted.edition) FROM inserted 
	                            WHERE inserted.Publisher_Id=Publishers.Publisher_Id),0)-
	                            COALESCE((SELECT SUM(deleted.edition) FROM deleted WHERE deleted.Publisher_Id=Publishers.Publisher_Id),0))

	WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION SELECT Publisher_Id FROM deleted);
END;
go


CREATE trigger book_amount_definition_update ON Books
AFTER update
AS
BEGIN
	if update(edition) AND NOT update(Publisher_id)
	begin

		UPDATE Publishers
		SET Publishers.total_edition=Publishers.total_edition+COALESCE((SELECT SUM(inserted.edition) FROM inserted 
	                            WHERE inserted.Publisher_Id=Publishers.Publisher_Id),0)-
	                            COALESCE((SELECT SUM(deleted.edition) FROM deleted WHERE deleted.Publisher_Id=Publishers.Publisher_Id),0)
		WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION SELECT Publisher_Id FROM deleted);

	end;

	if update(Publisher_id)
	begin

		UPDATE Publishers
		SET Publishers.book_amount=COALESCE((SELECT COUNT(DISTINCT Books.ISBN) FROM Books
									WHERE Books.Publisher_Id=Publishers.Publisher_Id),0),

            Publishers.issue_amount=Publishers.issue_amount+COALESCE((SELECT COUNT(*) FROM inserted 
	                            WHERE inserted.Publisher_Id=Publishers.Publisher_Id),0)-
	                            COALESCE((SELECT COUNT(*) FROM deleted WHERE deleted.Publisher_Id=Publishers.Publisher_Id),0),

			Publishers.total_edition=Publishers.total_edition+COALESCE((SELECT SUM(inserted.edition) FROM inserted 
	                            WHERE inserted.Publisher_Id=Publishers.Publisher_Id),0)-
	                            COALESCE((SELECT SUM(deleted.edition) FROM deleted WHERE deleted.Publisher_Id=Publishers.Publisher_Id),0)

		WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION SELECT Publisher_Id FROM deleted);

	end;
END;