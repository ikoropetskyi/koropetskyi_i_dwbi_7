﻿--1--
USE labor_sql;

;WITH ship_cte(ship_name, ship_class)
AS
(
  SELECT [name], class
  FROM ships
  WHERE launched>1940
)
SELECT sc.ship_name, c.country
FROM ship_cte AS sc
  INNER JOIN classes AS c
    ON sc.ship_class=c.class;

--2--
USE labor_sql;

;WITH pass_cte(id_psg,[name])
AS
(
  SELECT id_psg, [name]
  FROM passenger
  WHERE [name] LIKE 'M%'
),

pass_trip_cte(id,[name],trip_no)
AS
(
  SELECT psc.id_psg, psc.[name], pit.trip_no
  FROM pass_cte AS psc
    INNER JOIN pass_in_trip AS pit
	  ON psc.id_psg=pit.id_psg
)

SELECT id AS pass_id, [name] AS pass_name, COUNT(trip_no) AS all_trips,
       COUNT(DISTINCT trip_no) AS distinct_trips
FROM pass_trip_cte
GROUP BY id, [name];

--3--
USE labor_sql;
;WITH region_cte
AS
(
  SELECT region_id, id AS place_id, [name], 0 AS place_level
  FROM [geography]
  WHERE region_id IS NULL

  UNION ALL

  SELECT g.region_id, g.id, g.[name], rc.place_level+1
  FROM [geography] AS g
    INNER JOIN region_cte AS rc
	  ON g.region_id=rc.place_id
)

SELECT * FROM region_cte
WHERE place_level=1;

--4--
USE labor_sql;

DECLARE @region AS VARCHAR(50);
SET @region='Ivano-Frankivsk';

;WITH region_cte
AS
(
  SELECT region_id, id AS place_id, [name], -1 AS place_level
  FROM [geography]
  WHERE [name]=@region

  UNION ALL

  SELECT g.region_id, g.id, g.[name], rc.place_level+1
  FROM [geography] AS g
    INNER JOIN region_cte AS rc
	  ON g.region_id=rc.place_id
)

SELECT * FROM region_cte
WHERE place_level>=0;


--5--
;WITH rec_cte(n)
AS
(
  SELECT 1 AS n
  
  UNION ALL

  SELECT n+1
  FROM rec_cte
  WHERE n<10000
)

SELECT n FROM rec_cte
OPTION (MAXRECURSION 10000);

--6--
;WITH rec_cte(n)
AS
(
  SELECT 1 AS n
  
  UNION ALL

  SELECT n+1
  FROM rec_cte
  WHERE n<100000
)

SELECT n FROM rec_cte
OPTION (MAXRECURSION 0);

--7--
;WITH dates_cte([date])
AS
(
  SELECT CAST(CONCAT(YEAR(GETDATE()),'0101') AS datetime) AS [date]
  
  UNION ALL
  
  SELECT DATEADD(d,1,[date])
  FROM dates_cte
  WHERE [date] < CONCAT(YEAR(GETDATE()),'1231')
)
SELECT COUNT([date]) AS n_sat_sun
FROM dates_cte 
WHERE DATENAME(dw,[date]) = 'Sunday' OR DATENAME(dw,[date]) = 'Saturday'
OPTION (MAXRECURSION 0);

--8--
USE labor_sql;
SELECT DISTINCT pr.maker
FROM product AS pr
WHERE pr.[type]='PC' AND 
      pr.maker NOT IN (SELECT maker
                       FROM product
					   WHERE [type]='Laptop');

--9--
USE labor_sql;
SELECT DISTINCT pr.maker
FROM product AS pr
WHERE pr.[type]='PC' AND 
      pr.maker<>ALL(SELECT maker
                    FROM product
					WHERE [type]='Laptop');

--10--
USE labor_sql;
SELECT DISTINCT pr.maker
FROM product AS pr
WHERE pr.[type]='PC' AND 
      NOT pr.maker=ANY(SELECT maker
                    FROM product
					WHERE [type]='Laptop');

--11--
USE labor_sql;
SELECT DISTINCT pr.maker
FROM product AS pr
WHERE pr.[type]='PC' AND 
      pr.maker IN (SELECT maker
                   FROM product
				   WHERE [type]='Laptop');


--12--
USE labor_sql;
SELECT DISTINCT pr.maker
FROM product AS pr
WHERE pr.[type]='PC' AND 
      NOT pr.maker<>ALL(SELECT maker
                    FROM product
					WHERE [type]='Laptop');

--13--
USE labor_sql;
SELECT DISTINCT pr.maker
FROM product AS pr
WHERE pr.[type]='PC' AND 
      pr.maker=ANY(SELECT maker
                    FROM product
					WHERE [type]='Laptop');

--14--
USE labor_sql;
SELECT DISTINCT maker
FROM product
WHERE [type]='pc' 
       AND maker NOT IN(
						SELECT maker
						FROM product
						WHERE [type]='pc' 
							  AND model<>ALL(SELECT model
											 FROM pc)
						);

--15--
USE labor_sql;
;WITH c
AS
(
	SELECT country, class,
	       CASE country
		     WHEN 'Ukraine' THEN 1
			 ELSE 0
		   END AS ukr_id
	FROM classes
)

SELECT country, class
FROM c
WHERE ukr_id=CASE
               WHEN (SELECT SUM(ukr_id) FROM c)>0 THEN 1
			   ELSE 0
			 END;


--16--
USE labor_sql;
SELECT o.ship, o.battle, (SELECT [date] FROM battles WHERE [name]=o.battle) AS [date]
FROM outcomes AS o
WHERE o.result='damaged'
      AND EXISTS (SELECT 1
	              FROM outcomes AS o2
				  WHERE o2.ship=o.ship AND 
				  (SELECT [date] FROM battles WHERE [name]=o2.battle)>(SELECT [date] FROM battles WHERE [name]=o.battle) 
				  );

--17--
USE labor_sql;
SELECT DISTINCT pr1.maker
FROM product AS pr1
WHERE [type]='PC'
      AND NOT EXISTS (SELECT 1
	                  FROM product AS pr2
					    LEFT JOIN pc
						  ON pr2.model=pc.model
					  WHERE pr2.maker=pr1.maker AND pr2.[type]='PC' AND pc.model IS NULL);

--18--
USE labor_sql;
SELECT DISTINCT maker
FROM product
WHERE [type]='Printer' 
      AND maker IN (SELECT maker
	                FROM product
					WHERE model IN (SELECT model FROM PC
									WHERE speed=(SELECT MAX(speed) FROM PC)
									)
					);

--19--
USE labor_sql;
SELECT s.class
FROM outcomes AS o
  INNER JOIN ships AS s
    ON o.ship=s.[name]
WHERE o.result='sunk'

UNION

SELECT c.class
FROM outcomes AS o
  INNER JOIN classes AS c
    ON o.ship=c.class
WHERE o.result='sunk';

--20--
USE labor_sql;
SELECT model, price
FROM printer
WHERE price=(SELECT MAX(price) FROM printer);

--21--
USE labor_sql;
SELECT 'Laptop' AS [type], model, speed 
FROM laptop
WHERE speed<ALL(SELECT speed FROM PC);

--22--
USE labor_sql;

SELECT DISTINCT maker, (SELECT MIN(price) FROM printer WHERE color='y')  AS price
FROM product
WHERE model IN(
			SELECT model
			FROM printer
			WHERE color='y' 
				  AND price=(SELECT MIN(price)
							 FROM printer
							 WHERE color='y'));

--23--
USE labor_sql;
SELECT o.battle, c.country, COUNT(o.ship) AS ship_number
FROM outcomes AS o
  INNER JOIN ships AS s
    ON o.ship=s.[name]
	 INNER JOIN classes AS c
	   ON s.class=c.class
GROUP BY c.country, o.battle
HAVING COUNT(o.ship)>1;

--24--
USE labor_sql;
SELECT DISTINCT maker, 
       (SELECT COUNT(code) FROM pc WHERE model IN (SELECT model FROM product WHERE maker=p.maker)) AS pc,
	   (SELECT COUNT(code) FROM laptop WHERE model IN (SELECT model FROM product WHERE maker=p.maker)) AS laptop,
	   (SELECT COUNT(code) FROM printer WHERE model IN (SELECT model FROM product WHERE maker=p.maker)) AS printer
FROM product AS p;

--25--
USE labor_sql;
;WITH C
AS
(
SELECT DISTINCT maker, 
       (SELECT COUNT(code) FROM pc WHERE model IN (SELECT model FROM product WHERE maker=p.maker)) AS pc
FROM product AS p)

SELECT maker,
       CASE
	      WHEN pc=0 THEN 'no'
		  ELSE 'yes(' +CAST(pc AS VARCHAR(10)) + ')'
	   END AS pc  
FROM C;

--26--
USE labor_sql;
;WITH c
AS
(
	SELECT point, [date]
	FROM income_o

	UNION

	SELECT point, [date]
	FROM outcome_o
)

SELECT c.point, c.[date], COALESCE([io].inc,0) AS inc, COALESCE(oo.[out],0) AS [out]
FROM c
  LEFT JOIN income_o AS [io]
    ON c.point=[io].point AND c.[date]=[io].[date]
	 LEFT JOIN outcome_o AS oo
	   ON c.point=oo.point AND c.[date]=oo.[date];

--27--
USE labor_sql;
SELECT s.[name], c.numGuns, c.bore, c.displacement,
       c.[type], c.country, s.launched, c.class
FROM ships AS s
  INNER JOIN classes AS c
    ON s.class=c.class 
WHERE 
     ((CASE c.numGuns WHEN 8 THEN 1 ELSE 0 END) +
	 (CASE c.bore WHEN 15 THEN 1 ELSE 0 END) +
	 (CASE c.displacement WHEN 32000 THEN 1 ELSE 0 END) +
	 (CASE c.[type] WHEN 'bb' THEN 1 ELSE 0 END) +
	 (CASE c.country WHEN 'USA' THEN 1 ELSE 0 END) +
	 (CASE s.launched WHEN 1915 THEN 1 ELSE 0 END) +
	 (CASE c.class WHEN 'Kon' THEN 1 ELSE 0 END))>3;

--28--
USE labor_sql;
;WITH point_date_cte
AS
(
  SELECT point, [date]
  FROM outcome_o

  UNION

  SELECT point, [date]
  FROM outcome
),

 outcome_cte
 AS
 (
   SELECT point, [date], SUM([out]) AS [out]
   FROM outcome
   GROUP BY point, [date]
 ),


 aggr_out_cte
 AS
 (
   SELECT pdc.point, pdc.[date], COALESCE(oo.[out],0) AS once_day_out, COALESCE(oc.[out],0) AS more_once_out
   FROM point_date_cte pdc
     LEFT JOIN outcome_o AS oo
	   ON pdc.point=oo.point and pdc.[date]=oo.[date]
	     LEFT JOIN outcome_cte AS oc
		   ON pdc.point=oc.point and pdc.[date]=oc.[date]
 )

 SELECT point, [date],
        CASE
		 WHEN once_day_out>more_once_out THEN 'once a day'
		 WHEN once_day_out<more_once_out THEN 'more than once a day'
		 ELSE 'both'
		END AS lider
 FROM aggr_out_cte;

--29--
USE labor_sql;
SELECT pr.maker, pr.model, pr.[type], pc.price
FROM product AS pr
  INNER JOIN pc
    ON pr.model=pc.model AND pr.maker='B'

UNION ALL

SELECT pr.maker, pr.model, pr.[type], prn.price
FROM product AS pr
  INNER JOIN printer AS prn
    ON pr.model=prn.model AND pr.maker='B'

UNION ALL

SELECT pr.maker, pr.model, pr.[type], l.price
FROM product AS pr
  INNER JOIN laptop AS l
    ON pr.model=l.model AND pr.maker='B';

--30--
USE labor_sql;
SELECT [name], class
FROM ships
WHERE class=[name]

UNION

SELECT o.ship, c.class
FROM outcomes AS o
  INNER JOIN classes AS c
    ON o.ship=c.class;

--31--
USE labor_sql;
;WITH ship_cte
AS
(
	SELECT class, [name]
	FROM ships

	UNION

	SELECT c.class, o.ship
	FROM outcomes AS o
	  INNER JOIN classes AS c
		ON o.ship=c.class
)
SELECT class
FROM ship_cte
GROUP BY class
HAVING COUNT([name])=1;

--32--
USE labor_sql;
SELECT [name]
FROM ships
WHERE launched<1942

UNION

SELECT o.ship
FROM outcomes AS o
  INNER JOIN battles AS b
    ON o.battle=b.[name]
WHERE b.[date]<'19420101';