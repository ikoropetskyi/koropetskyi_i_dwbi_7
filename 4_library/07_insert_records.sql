﻿USE I_Koropetskyi_Library;
------------------------------
SELECT * FROM Authors;
SELECT * FROM Authors_log;

INSERT INTO Authors (Author_Id,Name,URL)
VALUES
      (NEXT VALUE FOR Author,'SHEVCHENCKO TARAS',DEFAULT),
	  (NEXT VALUE FOR Author,'MAZUR DENIS','mazur_n.com.ua'),
	  (NEXT VALUE FOR Author,'IHOR KOROPETSKTI',DEFAULT),
	  (NEXT VALUE FOR Author,'SHEVCHUK TARAS',DEFAULT),
	  (NEXT VALUE FOR Author,'Poul Anderson',DEFAULT),
	  (NEXT VALUE FOR Author,'F.Anstey',DEFAULT),
	  (NEXT VALUE FOR Author,'Tom Arden','www.some_site.en'),
	  (NEXT VALUE FOR Author,'Danilo Arona',DEFAULT),
	  (NEXT VALUE FOR Author,'Sarah Ash',DEFAULT),
	  (NEXT VALUE FOR Author,'Mike Ashley','www.ma.us'),
	  (NEXT VALUE FOR Author,'Ilona Andrew',DEFAULT),
	  (NEXT VALUE FOR Author,'Nancy Asire',DEFAULT),
	  (NEXT VALUE FOR Author,'Robert Lynn Asprin','www.rla.en'),
	  (NEXT VALUE FOR Author,'Amelia Atwater-Rhodes',DEFAULT),
	  (NEXT VALUE FOR Author,'Anselm Audley','www.my.site.com'),
	  (NEXT VALUE FOR Author,'Fiona Avery',DEFAULT),
	  (NEXT VALUE FOR Author,'Victoria Aveyard','www.writers.in.ua'),
	  (NEXT VALUE FOR Author,'Frank Baum',DEFAULT),
	  (NEXT VALUE FOR Author,'Peter S. Beagle',DEFAULT),
	  (NEXT VALUE FOR Author,'Frank Beddor',DEFAULT);

SELECT * FROM Authors;
SELECT * FROM Authors_log;

-------------------------------------------------
SELECT * FROM Publishers;

INSERT INTO Publishers (Publisher_Id,Name,URL)
VALUES
      (NEXT VALUE FOR Publisher,'Zoria',DEFAULT),
	  (NEXT VALUE FOR Publisher,'Ranok','RANOK.com.ua'),
	  (NEXT VALUE FOR Publisher,'vsesvit',DEFAULT),
	  (NEXT VALUE FOR Publisher,'start',DEFAULT),
	  (NEXT VALUE FOR Publisher,'NEW_ERA',DEFAULT),
	  (NEXT VALUE FOR Publisher,'AGE',DEFAULT),
	  (NEXT VALUE FOR Publisher,'Dignity','www.dd.en'),
	  (NEXT VALUE FOR Publisher,'Marcelo',DEFAULT),
	  (NEXT VALUE FOR Publisher,'Abraham',DEFAULT),
	  (NEXT VALUE FOR Publisher,'Viper','www.ma.us'),
	  (NEXT VALUE FOR Publisher,'Desktop',DEFAULT),
	  (NEXT VALUE FOR Publisher,'Shame',DEFAULT),
	  (NEXT VALUE FOR Publisher,'Loft','www.URA.en'),
	  (NEXT VALUE FOR Publisher,'Dztn',DEFAULT),
	  (NEXT VALUE FOR Publisher,'LLL','www.my.lll_k.com'),
	  (NEXT VALUE FOR Publisher,'Doroty',DEFAULT),
	  (NEXT VALUE FOR Publisher,'Agedf','www.pUBLISHERS.in.ua'),
	  (NEXT VALUE FOR Publisher,'Memorial',DEFAULT),
	  (NEXT VALUE FOR Publisher,'Henessy',DEFAULT),
	  (NEXT VALUE FOR Publisher,'Dawn',DEFAULT);

SELECT * FROM Publishers;

-------------------------------------------------------------
SELECT * FROM Books;

INSERT INTO Books(ISBN,Publisher_Id,URL,Price)
VALUES
      ('111-11111-22222-1',1,'www.book_1.com.us',10.00),
	  ('111-11111-22222-2',1,'www.book_2.com.us',20.00),
	  ('111-11111-22222-3',2,'www.book_3.com.us',56.00),
	  ('111-11111-22222-4',3,'www.book_4.com.us',33.00),
	  ('111-11111-22222-5',4,'www.book_5.com.us',55.00),
	  ('111-11111-22222-6',5,'www.book_6.com.us',65.00),
	  ('111-11111-22222-7',6,'www.book_7.com.us',12.00),
	  ('111-11111-22222-8',7,'www.book_8.com.us',14.00),
	  ('111-11111-22222-9',8,'www.book_9.com.us',16.00),
	  ('111-11111-22221-1',9,'www.book_10.com.us',55.00),
	  ('111-11111-22223-1',10,'www.book_11.com.us',234.00),
	  ('111-11111-22224-1',11,'www.book_12.com.us',455.00),
	  ('111-11111-22225-1',12,'www.book_13.com.us',433.00),
	  ('111-11111-22226-1',13,'www.book_14.com.us',554.00),
	  ('111-11111-22227-1',14,'www.book_15.com.us',335.00),
	  ('111-11111-22228-1',15,'www.book_16.com.us',664.00),
	  ('111-11111-22229-1',5,'www.book_17.com.us',64.00),
	  ('111-11111-22233-1',4,'www.book_18.com.us',35.00),
	  ('111-11111-22234-1',3,'www.book_19.com.us',765.00),
	  ('111-11111-22235-1',2,'www.book_20.com.us',134.00)

SELECT * FROM Books;

-----------------------------------------------------------
SELECT * FROM BooksAuthors;

INSERT INTO BooksAuthors(BooksAuthors_id,ISBN,Author_Id,Seq_No)
VALUES
	  (1,'111-11111-22222-1',1,2),
	  (2,'111-11111-22222-2',2,1),
	  (3,'111-11111-22222-3',3,3),
	  (4,'111-11111-22222-4',4,5),
	  (5,'111-11111-22222-5',5,2),
	  (6,'111-11111-22222-6',6,1),
	  (7,'111-11111-22222-7',7,3),
	  (8,'111-11111-22222-8',8,3),
	  (9,'111-11111-22222-9',9,1),
	  (10,'111-11111-22221-1',10,2),
	  (11,'111-11111-22223-1',11,1),
	  (12,'111-11111-22224-1',12,6),
	  (13,'111-11111-22225-1',13,2),
	  (14,'111-11111-22226-1',15,2),
	  (15,'111-11111-22227-1',15,1),
	  (16,'111-11111-22227-1',1,1),
	  (17,'111-11111-22226-1',2,3),
	  (18,'111-11111-22225-1',4,4),
	  (19,'111-11111-22224-1',6,5),
	  (20,'111-11111-22223-1',10,1);


SELECT * FROM BooksAuthors;
