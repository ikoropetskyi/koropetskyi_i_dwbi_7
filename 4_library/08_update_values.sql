﻿USE I_Koropetskyi_Library;
GO
---------------------
SELECT * FROM Authors;

UPDATE Authors
  SET Name='NEW_'+Name
  WHERE Author_Id IN (2,4,6,8,10,12,14,16,18,10);

SELECT * FROM Authors;
SELECT * FROM Authors_log;

-----------------------
SELECT * FROM Books;

UPDATE Books
  SET Price=Price+100
  WHERE Price>50;

SELECT * FROM Books;

-----------------------
SELECT * FROM Publishers;

UPDATE Publishers
  SET Name=Name+URL
  WHERE Publisher_Id<=10;

SELECT * FROM Publishers;
------------------------

SELECT * FROM BooksAuthors;

UPDATE BooksAuthors
  SET Seq_No+=1
  WHERE BooksAuthors_id NOT IN(1,4,6,7,10)

SELECT * FROM BooksAuthors;

