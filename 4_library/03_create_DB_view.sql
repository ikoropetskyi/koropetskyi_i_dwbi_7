﻿USE I_Koropetskyi_Library;
GO

DROP VIEW IF EXISTS I_Koropetskyi_Library_view;
GO

CREATE VIEW I_Koropetskyi_Library_view
AS
SELECT BA.ISBN, A.Name AS author_name, A.URL AS author_url, B.URL AS book_url,
       B.Price AS book_price, P.Name AS publisher_name
FROM BooksAuthors AS BA
  INNER JOIN Authors AS A 
  ON BA.Author_Id=A.Author_Id
    INNER JOIN Books AS B
	ON BA.ISBN=B.ISBN
	  INNER JOIN Publishers AS P
	    ON B.Publisher_Id=P.Publisher_Id;
GO

SELECT *
FROM I_Koropetskyi_Library_view;