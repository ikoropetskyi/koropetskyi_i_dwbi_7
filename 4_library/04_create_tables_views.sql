USE I_Koropetskyi_Library;
GO

DROP VIEW IF EXISTS Authors_view;
DROP VIEW IF EXISTS Books_view;
DROP VIEW IF EXISTS BooksAuthors_view;
DROP VIEW IF EXISTS Publishers_view;
DROP VIEW IF EXISTS Authors_log_view;
GO

CREATE VIEW Authors_view
AS
SELECT * FROM Authors;
GO

CREATE VIEW Books_view
AS
SELECT * FROM Books;
GO

CREATE VIEW BooksAuthors_view
AS
SELECT * FROM BooksAuthors;
GO

CREATE VIEW Publishers_view
AS
SELECT * FROM Publishers;
GO

CREATE VIEW Authors_log_view
AS
SELECT * FROM Authors_log;
GO

SELECT * FROM Authors_view;
SELECT * FROM Books_view;
SELECT * FROM BooksAuthors_view;
SELECT * FROM Publishers_view;
SELECT * FROM Authors_log_view;