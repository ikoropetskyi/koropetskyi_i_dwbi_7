﻿USE I_Koropetskyi_Library;
GO
---------------------
SELECT * FROM Authors;

DELETE FROM Authors
WHERE Author_Id>=16;

SELECT * FROM Authors;
SELECT * FROM Authors_log;

-----------------------
SELECT * FROM Books;

  DELETE FROM Books
  WHERE ISBN IN ('111-11111-22228-1','111-11111-22233-1','111-11111-22234-1','111-11111-22235-1','111-11111-22229-1');

SELECT * FROM Books;

-----------------------
SELECT * FROM Publishers;

  DELETE FROM Publishers
  WHERE Publisher_Id>=16;

SELECT * FROM Publishers;
------------------------

SELECT * FROM BooksAuthors;

  DELETE FROM BooksAuthors
  WHERE BooksAuthors_id%2=0;

SELECT * FROM BooksAuthors;
GO

-----------------------
SELECT * FROM Authors_log;

DELETE FROM Authors_log
WHERE operation_type='U';

SELECT * FROM Authors_log;
GO