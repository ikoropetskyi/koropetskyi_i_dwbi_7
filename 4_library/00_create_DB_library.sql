﻿USE master;
GO

DROP DATABASE IF EXISTS I_Koropetskyi_Library;
GO

CREATE DATABASE I_Koropetskyi_Library;
GO

ALTER DATABASE I_Koropetskyi_Library 
  ADD FILEGROUP [DATA];
GO

ALTER DATABASE I_Koropetskyi_Library 
ADD FILE (NAME = N'DATA_Library', 
    FILENAME = N'E:\sql2014\MSSQL12.SQLEXPRESS\MSSQL\DATA\DATA_Library.mdf') TO FILEGROUP [DATA];
GO

USE I_Koropetskyi_Library;
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'DATA') 
	ALTER DATABASE I_Koropetskyi_Library MODIFY FILEGROUP [DATA] DEFAULT
GO