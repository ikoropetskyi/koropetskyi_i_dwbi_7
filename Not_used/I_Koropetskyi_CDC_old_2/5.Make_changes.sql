USE I_Koropetskyi_CDC;

--MAKE DELETES--
DELETE FROM dbo.Koropetskyi_DimCurrency_CDC
WHERE CurrencyKey<15;
GO


--MAKE INSERTS--
SET IDENTITY_INSERT dbo.Koropetskyi_DimCurrency_CDC ON;
GO

INSERT INTO Koropetskyi_DimCurrency_CDC (CurrencyKey,CurrencyAlternateKey,CurrencyName)
VALUES (101, 'AAA','1 Currency'),
       (102, 'BBB','2 Currency'),
	   (103, 'CCC','3 Currency'),
	   (104, 'DDD','4 Currency'),
	   (105, 'EEE','5 Currency'),
	   (106, 'FFF','6 Currency'),
	   (107, 'GGG','7 Currency'),
	   (108, 'HHH','8 Currency'),
	   (109, 'III','9 Currency'),
	   (110, 'JJJ','10 Currency');
GO

SET IDENTITY_INSERT dbo.Koropetskyi_DimCurrency_CDC OFF;
GO

--MAKE UPDATES--
UPDATE dbo.Koropetskyi_DimCurrency_CDC
SET CurrencyName='Test_name'
WHERE CurrencyKey>35 AND CurrencyKey<=40;
GO