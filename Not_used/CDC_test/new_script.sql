
--drop test database if exists
IF db_id('I_Koropetskyi_test') IS NOT NULL
DROP DATABASE [I_Koropetskyi_test];
GO

--create test database--
CREATE DATABASE [I_Koropetskyi_test];
GO

--CREATE SOURCE TABLE 
USE [I_Koropetskyi_test];
GO
IF OBJECT_ID('dbo.DimCurrency_CDC_Source','U') IS NOT NULL
  DROP TABLE [dbo].[DimCurrency_CDC_Source];
GO

SELECT [CurrencyAlternateKey], [CurrencyName]
INTO [dbo].[DimCurrency_CDC_Source]
FROM [AdventureWorksDW2014].[dbo].[DimCurrency]
WHERE CurrencyKey<=40;
GO

--CREATE PRIMARY KEY ON SOURCE TABLE
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID('[dbo].[DimCurrency_CDC_Source]') AND [name] = 'PK_DimCurrency_CDC_Source')
  ALTER TABLE [dbo].[DimCurrency_CDC_Source] ADD CONSTRAINT [PK_DimCurrency_CDC_Source] PRIMARY KEY CLUSTERED
(
    [CurrencyAlternateKey]
)
GO

--CREATE DESTINITION TABLE--
IF OBJECT_ID('dbo.DimCurrency_Destination','U') IS NOT NULL
  DROP TABLE [dbo].[DimCurrency_Destination];
GO

SELECT TOP 0 * INTO [dbo].[DimCurrency_Destination]
FROM [dbo].[DimCurrency_CDC_Source];


--ENABLE CDC ON DATABASE--
USE [I_Koropetskyi_test];
GO

EXEC sys.sp_cdc_enable_db;
GO



--ENABLE CDC ON SOURCE TABLE--
EXEC sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name = N'DimCurrency_CDC_Source',
@role_name = N'cdc_Admin',
@supports_net_changes = 1;

GO


--create cdc states table
/*USE [I_Koropetskyi_CDC];
GO
CREATE TABLE [dbo].[cdc_states] 
 ([name] [nvarchar](256) NOT NULL, 
 [state] [nvarchar](256) NOT NULL) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [cdc_states_name] ON 
 [dbo].[cdc_states] 
 ( [name] ASC ) 
 WITH (PAD_INDEX  = OFF) ON [PRIMARY]
GO*/











