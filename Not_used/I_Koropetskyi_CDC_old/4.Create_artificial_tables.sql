--CREATE ARTIFICIAL TABLES--
USE I_Koropetskyi_CDC;
GO

IF OBJECT_ID('dbo.Koropetskyi_DimCurrency_CDC_updates','U') is null
BEGIN
   SELECT TOP 0 * INTO dbo.Koropetskyi_DimCurrency_CDC_updates
   FROM dbo.Koropetskyi_DimCurrency_Destination
END;
go

IF OBJECT_ID('dbo.Koropetskyi_DimCurrency_CDC_deletes','U') is null
BEGIN
   SELECT TOP 0 * INTO dbo.Koropetskyi_DimCurrency_CDC_deletes
   FROM  dbo.Koropetskyi_DimCurrency_Destination
END;
go