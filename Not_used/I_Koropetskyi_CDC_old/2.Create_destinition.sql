--CREATE DESTINITION TABLE--
USE I_Koropetskyi_CDC;
IF OBJECT_ID('dbo.Koropetskyi_DimCurrency_Destination','U') IS NOT NULL
  DROP TABLE [dbo].[Koropetskyi_DimCurrency_Destination];
GO

SELECT TOP 0 * INTO [dbo].[Koropetskyi_DimCurrency_Destination]
FROM [dbo].[Koropetskyi_DimCurrency_CDC];