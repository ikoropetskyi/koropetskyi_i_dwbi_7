﻿use people_ua_db;

--fill table with regions
insert into region([name],main_city,part_of_country,number_of_universities)
values('Одеська','Одеса','Південь',4),
      ('Дніпропетровська','Дніпро','Центр',2),
	  ('Чернігівська','Чернігів','Північ',1),
	  ('Харківська','Харків','Схід',5),
	  ('Житомирська','Житомир','Північ',2),
	  ('Полтавська','Полтава','Центр',2),
	  ('Херсонська','Херсон','Південь',3),
	  ('Київська','Київ','Північ',10),
	  ('Запорізька','Запоріжжя','Центр',2),
	  ('Луганська','Луганськ','Схід',1),
	  ('Донецька','Донецьк','Схід',3),
	  ('Вінницька','Вінниця','Центр',2),
	  ('АР Крим','Сімферополь','Південь',1),
	  ('Миколаївська','Миколаїв','Південь',4),
	  ('Кіровоградська','Кропивницький','Центр',1),
	  ('Сумська','Суми','Північ',3),
	  ('Львівська','Львів','Захід',10),
	  ('Черкаська','Черкаси','Центр',1),
	  ('Хмельницька','Хмельницьк','Центр',1),
	  ('Волинська','Луцьк','Захід',1),
	  ('Рівненська','РІвне','Захід',2),
	  ('Івано-Франківська','Івано-Франківськ','Захід',1),
	  ('Тернопільська','Торнопіль','Захід',2),
	  ('Закарпатська','Ужгород','Захід',2),
	  ('Чернівецька','Чернівці','Захід',3);
go


--start of inserting into main table with all people
drop table if exists women_names;
drop table if exists men_names;


--new table, only women names
create table women_names
(id int not null primary key identity,
[name] nvarchar(20) not null);
go

-- new table, only men names
create table men_names
(id int not null primary key identity,
[name] nvarchar(20) not null);
go

--insert data to new tables
insert into women_names([name])
select [name] from name_list_identity
where sex='w';

insert into men_names([name])
select [name] from name_list_identity
where sex='m';
go

declare @surname as nvarchar(20),
        @amount as int,
		@sex as nchar(1),
		@n_women_names as int,
		@n_men_names as int,
		@rand as int,
		@rand_mame as nvarchar(20),
		@rand_birthday as Date,
		@rand_region_id as tinyint,
		@new_sex as nchar(1),
		@n_all_names as int,
		@counter as int

set @n_women_names=(select max(id) from women_names);
set @n_men_names=(select max(id) from men_names);
set @n_all_names=(select max(id) from name_list_identity);
set @counter=1;

declare c cursor
for select surname, amount, sex from surname_list_identity;

open c;
fetch next from c into @surname, @amount,@sex;

while @@FETCH_STATUS=0
  begin
    while @amount>0
	  begin
	    set @rand_birthday=(select dateadd(day,ABS((CHECKSUM(NEWID()))) % 27758,'19250101'))
	    set @rand_region_id=(ABS((CHECKSUM(NEWID()))) % 25+1)
	    if @sex='m'
		  begin
			set @rand=(ABS((CHECKSUM(NEWID()))) % @n_men_names+1)
			set @rand_mame=(SELECT TOP 1 name FROM men_names WHERE id >= @rand)
	        INSERT INTO peoples(surname,[name],sex,birthday,region_of_birth_id)
	        VALUES(@surname,@rand_mame,'m',@rand_birthday,@rand_region_id)
		  end
		  
		  if @sex='w'
		  begin
			set @rand=(ABS((CHECKSUM(NEWID()))) % @n_women_names+1)
			set @rand_mame=(SELECT TOP 1 name FROM women_names WHERE id >= @rand)
	        INSERT INTO peoples(surname,[name],sex,birthday,region_of_birth_id)
	        VALUES(@surname,@rand_mame,'w',@rand_birthday,@rand_region_id)
		  end

		  if @sex is null
		    begin
			  set @rand=(ABS((CHECKSUM(NEWID()))) % @n_all_names+1)
			  set @rand_mame=(SELECT [name] FROM name_list_identity WHERE id = @rand)
			  set @new_sex=(SELECT sex FROM name_list_identity WHERE id = @rand)
	          INSERT INTO peoples(surname,[name],sex,birthday,region_of_birth_id)
	          VALUES(@surname,@rand_mame,@new_sex,@rand_birthday,@rand_region_id)
			end
	  
	  SET @amount=@amount-1
	  print 'now row'+cast(@counter as varchar(10))
	  set @counter=@counter+1
	  end
  fetch next from c into @surname, @amount,@sex;
  end

close c;
deallocate c;