﻿use people_ua_db;

--insert into two other tables random data

declare @counter as int;
set @counter=1;


while @counter<=200
 begin
	insert into university([name],found,region_id)
	values('university'+rtrim(cast(@counter as varchar(3))), dateadd(day,ABS((CHECKSUM(NEWID()))) % 50000,'18500101'),ABS((CHECKSUM(NEWID()))) % 25+1)
	set @counter=@counter+1;
 end

declare @n_people as int, @n_university as int;
set @counter=1;
set @n_people=(select max(id) from peoples);
set @n_university=(select max(id) from university);

 while @counter<=10000
   begin
     insert into university_graduates(people_id,university_id,graduate_date,specialization)
     values(ABS((CHECKSUM(NEWID()))) % @n_people+1,ABS((CHECKSUM(NEWID()))) % @n_university+1,dateadd(day,ABS((CHECKSUM(NEWID()))) % 25000,'19450101'),'specailization'+rtrim(cast(@counter as varchar(6))))
     set @counter=@counter+1;
   end
