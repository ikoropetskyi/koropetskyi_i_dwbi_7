﻿use people_ua_db;

create clustered index ixc_region_id on region(id)
with (fillfactor=40, pad_index=on);


create clustered index ixc_people_id on peoples(id)
with (fillfactor=30, pad_index=on);


create unique index ixu_region_name on region([name])
with (fillfactor=20, pad_index=on);


create nonclustered index ixn_people_name on peoples([name])
with (fillfactor=50, pad_index=on);

create nonclustered index ixn_people_birthday_sex on peoples(birthday)
include(sex);

