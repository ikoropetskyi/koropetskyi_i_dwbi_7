﻿use people_ua_db;
go

declare @counter as int;
set @counter=1;

while @counter<100
  begin
    insert into region([name],main_city,part_of_country,number_of_universities)
    values('region'+rtrim(cast(@counter as varchar(5))), 'city'+rtrim(cast(@counter as varchar(5))),'not_known',
	       ABS((CHECKSUM(NEWID()))) % 10)
    set @counter=@counter+1
  end;

declare @n_region as int;
set @n_region=(select max(id) from region)

set @counter=1;
while @counter<1000000
  begin
    insert into peoples(surname,[name],sex,birthday,region_of_birth_id)
    values('surname'+rtrim(cast(@counter as varchar(7))),'name'+rtrim(cast(@counter as varchar(7))),
	       case when @counter%2=0 then 'w' else 'm' end, dateadd(day,ABS((CHECKSUM(NEWID()))) % 27758,'19250101'),
		   (ABS((CHECKSUM(NEWID()))) % @n_region+1))
    set @counter=@counter+1
  end;


  delete from peoples
  where id between 20000 and 40000 or region_of_birth_id=2;

  update peoples
  set birthday=dateadd(year,1,birthday)
  where sex='m';

  update region
  set [name]=[name]+'обл';




