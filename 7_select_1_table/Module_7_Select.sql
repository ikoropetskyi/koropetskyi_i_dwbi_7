﻿--1--
use labor_sql;
select distinct maker, [type]
from product
where [type]='laptop'
order by maker;

--2--
use labor_sql;
select model, ram, screen, price
from laptop
where price>1000
order by ram, price desc;

--3--
use labor_sql;
select code, model, color, [type], price
from printer
where color='y'
order by price desc;

--4--
use labor_sql;
select model,speed,hd,cd,price
from pc
where (cd='12x' or cd='24x') and price<600
order by speed desc;

--5--
use labor_sql;
select [name], class
from ships
where [name]=class
order by [name];

--6--
use labor_sql;
select code, model, speed, ram, hd, cd, price
from pc
where speed>=500 and price<800
order by price desc;

--7--
use labor_sql;
select code, model, color, [type], price
from printer
where [type]<>'Matrix' and price<300
order by [type] desc;

--8--
use labor_sql;
select model, speed
from pc
where price between 400 and 600
order by hd;

--9--
use labor_sql;
select model, speed, hd, price
from laptop
where screen>=12
order by price desc;

--10--
use labor_sql;
select model, [type], price
from printer
where price<300
order by [type] desc;

--11--
use labor_sql;
select model, ram, price
from laptop
where ram=64
order by screen;

--12--
use labor_sql;
select model, ram, price
from pc
where ram>64
order by hd;

--13--
use labor_sql;
select model, speed, price
from pc
where speed between 500 and 750
order by hd desc;

--14--
use labor_sql;
select point, [date], [out]
from outcome_o
where [out]>2000
order by [date] desc;

--15--
use labor_sql;
select point, [date], inc
from income_o
where inc between 5000 and 10000
order by inc;

--16--
use labor_sql;
select code, point, [date], inc
from income
where point=1
order by inc;

--17--
use labor_sql;
select code, point, [date], [out]
from outcome
where point=2
order by [out];

--18--
use labor_sql;
select class, [type], country, numGuns, bore, displacement
from classes
where country='Japan'
order by [type] desc;

--19--
use labor_sql;
select [name], launched
from ships
where launched between 1920 and 1942
order by launched desc;

--20--
use labor_sql;
select ship, battle,result
from outcomes
where result<>'sunk' and  battle='Guadalcanal'
order by ship desc;

--21--
use labor_sql;
select ship, battle, result
from outcomes
where result='sunk'
order by ship desc;

--22--
use labor_sql;
--maybe the conditions of task for displacement should be not 40 but 40000 of tonn
select class, displacement
from classes
where displacement>=40
order by [type];

--23--
use labor_sql;
select trip_no, town_from, town_to
from trip
where town_from='London' or town_to='London'
order by time_out;

--24--
use labor_sql;
select trip_no, plane, town_from, town_to
from trip
where plane='TU-134'
order by time_out desc;

--25--
use labor_sql;
select trip_no, plane, town_from, town_to
from trip
where plane<>'IL-86'
order by plane;

--26--
use labor_sql;
select trip_no, town_from, town_to
from trip
where town_from<>'Rostov' and town_to<>'Rostov'
order by plane;

--27--
use labor_sql;
select model
from product
where model like '%1%1%' and [type]='pc';

--28--
use labor_sql;
select code, point, [date], [out]
from outcome
where month(date)=3;

--29--
use labor_sql;
select point, [date], [out]
from outcome_o
where day([date])=14;

--30--
use labor_sql;
select [name]
from ships
where [name] like 'W%n';

--31--
use labor_sql;
select [name]
from ships
where len([name])-len(replace([name],'e',''))=2;

--32--
use labor_sql;
select [name], launched
from ships
where [name] not like '%a';

--33--
use labor_sql;
select [name]
from battles
where trim([name]) like'%[- ]%[^c]';

--34--
use labor_sql;
select trip_no, id_comp, plane, town_from, town_to, time_out, time_in
from trip
where CAST(time_out AS time) between '12:00:00' AND '17:00:00';

--35--
use labor_sql;
select trip_no, id_comp, plane, town_from, town_to, time_out, time_in
from trip
where CAST(time_in AS time) between '17:00:00' AND '23:00:00';

--36--
use labor_sql;
select trip_no, id_comp, plane, town_from, town_to, time_out, time_in
from trip
where (CAST(time_in AS time) between '21:00:00' AND '23:59:59') or (CAST(time_in AS time) between '00:00:00' AND '10:00:00');

--37--
use labor_sql;
select distinct [date]
from pass_in_trip
where place like '1[a-d]';

--38--
use labor_sql;
select distinct [date]
from pass_in_trip
where place like '%[c]';

--39--
--surname could be not the second word
--Ex: Samuel L. Jackson, L is not a surname,
--we should check the last word in the name column
use labor_sql;
select distinct ltrim(reverse(substring(ltrim(REVERSE([name])),1,CHARINDEX(' ',ltrim(REVERSE([name])))))) as surname
from passenger
where left(ltrim(reverse(substring(ltrim(REVERSE([name])),1,CHARINDEX(' ',ltrim(REVERSE([name])))))),1)='C';

--40--
use labor_sql;
select distinct ltrim(reverse(substring(ltrim(REVERSE([name])),1,CHARINDEX(' ',ltrim(REVERSE([name])))))) as surname
from passenger
where left(ltrim(reverse(substring(ltrim(REVERSE([name])),1,CHARINDEX(' ',ltrim(REVERSE([name])))))),1)<>'j';

--41--
use labor_sql;
select concat('середня ціна=',cast(avg(price) as decimal(8,2)))
from laptop;

--42--
use labor_sql;
select concat('code: ',code) as code,concat('model: ',model) as model,concat('speed: ',speed) as speed,
       concat('ram: ',ram) as ram,concat('hd: ',hd) as hd,concat('cd: ', cd) as cd,
	   concat('price:',price) as price
from pc;

--43--
use labor_sql;
select distinct convert(char(10),[date],102) as [only_date]
from income;

--44--
use labor_sql;
select ship, battle,
       case result
	     when 'sunk' then 'потоплений'
		 when 'damaged' then 'пошкоджений'
		 when 'ok' then 'цілий'
		 else 'не відомо'
		end as result
from outcomes;

--45--
use labor_sql;
select trip_no, [date], id_psg,'ряд: '+left(place,len(place)-1) as [row],
       'місце: '+right(rtrim(place),1) as place
from pass_in_trip;

--46--
use labor_sql;
select trip_no, id_comp, plane, 'from '+rtrim(town_from)+' to '+rtrim(town_to) as towns,
       time_out, time_in 
from trip;

--47--
use labor_sql;
select left(trip_no,1)+right(trip_no,1)+left(id_comp,1)+right(id_comp,1)+
       left(plane,1)+right(rtrim(plane),1)+left(town_from,1)+right(rtrim(town_from),1)+
	   left(town_to,1)+right(rtrim(town_to),1)+left(convert(char(23),time_out,121),1)+right(convert(char(23),time_out,121),1)+
	   left(convert(char(23),time_in,121),1)+right(convert(char(23),time_in,121),1) as result
from trip;

--48--
use labor_sql;
select maker, count(model) as number_of_models
from product
where [type]='pc'
group by maker
having count(model)>1;

--49--
use labor_sql;
SELECT town, COUNT(DISTINCT trip_no) AS number_of_trips
FROM (SELECT town_from AS town, trip_no
	  FROM trip

      UNION

      SELECT town_to, trip_no
      FROM trip) AS A
GROUP BY town;

--50--
use labor_sql;
select [type], count(distinct model) as number_of_models
from printer
group by [type];

--51--
use labor_sql;
select cd,model,count(distinct cd) as number_of_cd,count(distinct model) as number_of_models
from pc
group by grouping sets(model,cd);

--52--
use labor_sql;
select  trip_no,convert(char(5),time_in-time_out,108) as flying_time
from trip;

--53--
use labor_sql;
select point, convert(char(10),[date],102), sum([out]) as sum_cash, min([out]) as min_cash, max([out]) as max_cash 
from outcome
group by grouping sets((point, [date]),(point));

--54--
use labor_sql;
select trip_no, left(place,len(place)-1) as [row], count(id_psg) number_of_sits
from pass_in_trip
group by trip_no, left(place,len(place)-1);

--55--
use labor_sql;
select count(*) as surname_on_sba
from passenger
where ltrim(reverse(substring(ltrim(REVERSE([name])),1,CHARINDEX(' ',ltrim(REVERSE([name])))))) like '[sba]%';