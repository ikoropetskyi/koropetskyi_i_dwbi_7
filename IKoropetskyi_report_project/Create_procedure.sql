USE AdventureWorksDW2014;
GO

IF OBJECT_ID('dbo.ufnSplit') IS NOT NULL
  DROP FUNCTION [dbo].[ufnSplit];
GO

IF OBJECT_ID('dbo.return_sales_by_county_product','P') IS NOT NULL
  DROP PROCEDURE dbo.return_sales_by_county_product;
GO

CREATE FUNCTION [dbo].[ufnSplit]
   (@RepParam nvarchar(max), @Delim char(1)= ',')
RETURNS @Values TABLE (Item nvarchar(100)) AS
  BEGIN
  DECLARE @chrind INT
  DECLARE @Piece nvarchar(100)
  SELECT @chrind = 1 
  WHILE @chrind > 0
    BEGIN
      SELECT @chrind = CHARINDEX(@Delim,@RepParam)
      IF @chrind  > 0
        SELECT @Piece = LEFT(@RepParam,@chrind - 1)
      ELSE
        SELECT @Piece = @RepParam
      INSERT  @Values(Item) VALUES(@Piece)
      SELECT @RepParam = RIGHT(@RepParam,LEN(@RepParam) - @chrind)
      IF LEN(@RepParam) = 0 BREAK
    END
  RETURN
  END 
GO


CREATE PROCEDURE dbo.return_sales_by_county_product
@country_list NVARCHAR(MAX),
@product_key_list NVARCHAR(MAX)
AS
BEGIN
  SELECT p.EnglishProductName AS Product, g.EnglishCountryRegionName AS Country, fi.SalesAmount AS SalesAmount
  FROM dbo.FactInternetSales as fi
    INNER JOIN dbo.DimCustomer AS c
      ON fi.CustomerKey=c.CustomerKey
	    INNER JOIN dbo.DimGeography as g
	      ON c.GeographyKey=g.GeographyKey
		    INNER JOIN dbo.DimProduct AS p
		      ON fi.ProductKey=p.ProductKey 
	WHERE g.EnglishCountryRegionName IN (select Item from dbo.ufnSplit(@country_list,',')) AND p.ProductKey IN (select Item from dbo.ufnSplit(@product_key_list,','))
END;