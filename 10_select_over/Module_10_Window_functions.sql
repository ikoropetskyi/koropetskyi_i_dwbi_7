﻿--1--
USE labor_sql;
SELECT ROW_NUMBER() OVER(ORDER BY id_comp, trip_no) AS num,
       trip_no, id_comp
FROM trip
ORDER BY id_comp, trip_no;

--2--
USE labor_sql;
SELECT ROW_NUMBER() OVER(PARTITION BY id_comp ORDER BY trip_no) AS num,
       trip_no, id_comp
FROM trip
ORDER BY id_comp, trip_no;

--3--
USE labor_sql;
;WITH printer_cte
AS
(
	SELECT model, color, [type], price, 
	       RANK() OVER(PARTITION BY [type] ORDER BY price) AS price_rank
	FROM printer
)
SELECT model, color, [type], price
FROM printer_cte
WHERE price_rank=1;

--4--
--version 1: using window functions
USE labor_sql;
;WITH pc_cte
AS
(
SELECT maker, ROW_NUMBER() OVER (PARTITION BY maker ORDER BY model) AS num_model
FROM product
WHERE [type]='pc'
)
SELECT DISTINCT maker
FROM pc_cte
WHERE num_model>2;

--version 2: using group by and having
USE labor_sql;
SELECT maker
FROM product
WHERE [type]='pc'
GROUP BY maker
HAVING COUNT(model)>2;

--5--
USE labor_sql;
;WITH price_cte
AS
(	SELECT price, DENSE_RANK() OVER(ORDER BY price DESC) AS price_rank
	FROM pc
)
SELECT DISTINCT price
FROM price_cte
WHERE price_rank=2;

--6--
--surname could be not the second word
--Ex: Samuel L. Jackson, L is not a surname,
--we should check the last word in the name column
USE labor_sql;
;WITH pass_cte 
AS
(
	SELECT id_psg, [name],
	       ltrim(reverse(substring(ltrim(REVERSE([name])),1,CHARINDEX(' ',ltrim(REVERSE([name])))))) as surname
	FROM passenger
)
SELECT id_psg, [name], 
       NTILE(3) OVER(ORDER BY surname) AS passenger_group 
FROM pass_cte
ORDER BY surname;

--7--
USE labor_sql;
SELECT code, model, speed, ram, hd, cd, price,
	   ROW_NUMBER() OVER(ORDER BY price DESC) AS id,
	   COUNT(*) OVER() AS row_total,
	   NTILE(CAST((SELECT CEILING(COUNT(*)/3.0) FROM PC) AS bigint)) OVER(ORDER BY price DESC) AS page_num,
	   (SELECT CEILING(COUNT(*)/3.0) FROM PC) AS page_total
FROM pc
ORDER BY price DESC;

--8--
--version 1: using 2 CTE + RANK
USE labor_sql;
;WITH union_cte
AS
(
	SELECT [out] AS [sum], 'out' AS [type], [date], point
	FROM outcome

	UNION ALL

	SELECT [out], 'out' AS [type], [date], point
	FROM outcome_o

	UNION ALL

	SELECT inc, 'inc' AS [type], [date], point
	FROM income

	UNION ALL

	SELECT inc, 'inc' AS [type], [date], point
	FROM income_o
),

max_sum_cte
AS
(
SELECT [sum], [type], [date], point,
       RANK() OVER(ORDER BY [sum] DESC) AS sum_rank
FROM union_cte
)

SELECT [sum] AS max_sum, [type], [date], point
FROM max_sum_cte
WHERE sum_rank=1;

--version 2: using 1 CTE + TOP
USE labor_sql;
;WITH union_cte
AS
(
	SELECT [out] AS [sum], 'out' AS [type], [date], point
	FROM outcome

	UNION ALL

	SELECT [out], 'out' AS [type], [date], point
	FROM outcome_o

	UNION ALL

	SELECT inc, 'inc' AS [type], [date], point
	FROM income

	UNION ALL

	SELECT inc, 'inc' AS [type], [date], point
	FROM income_o
)

SELECT TOP(1) WITH TIES [sum] AS max_sum, [type], [date], point
FROM union_cte
ORDER BY max_sum DESC;

--9--
USE labor_sql;
;WITH pc_cte
AS
(
	SELECT code, model, speed, ram, hd, cd, price,
		   AVG(price) OVER(PARTITION BY speed) AS local_price,
		   AVG(price) OVER() AS total_price
	FROM pc
)
SELECT code, model, speed, ram, hd, cd, price,
       price-local_price AS df_local_prica,
	   price-total_price AS df_total_price,
	   total_price
FROM pc_cte;
